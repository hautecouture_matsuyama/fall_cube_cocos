const FbMatchGame = require("FbMatchGame");
const GameOverProcessing = require("GameOverProcessing");

cc.Class({
    extends: cc.Component,

    properties: {
        fbMatchGame: FbMatchGame,
        gameOverProcessing: GameOverProcessing,

        titleScreenNode: cc.Node,
        gameScreenNode: cc.Node,

        myPhotoSpriteNode: cc.Sprite,
        opponentPhotoSpriteNode: cc.Sprite,

        textLabel1: cc.Label,
        opponentNameLabel: cc.Label,
        opponentScoreLabel: cc.Label,

        botSensorNode: cc.Node,
        objectContainerNode: cc.Node,
        ballContainerNode: cc.Node,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.photoChange(0);
        this.photoChange(1);
        this.textLabel1.string = "Play againt " + this.fbMatchGame.GetMathPlayerName() + '!';
        this.opponentNameLabel.string = this.fbMatchGame.GetMathPlayerName();
        this.opponentScoreLabel.string = this.fbMatchGame.GetMathScore() + ' Point';
    },

    photoChange (number) {
        const self = this;
        if (number === 0) {
            const photoUrl = self.fbMatchGame.GetMyPhoto();
            cc.loader.load(photoUrl, (err, texture) => {
                self.myPhotoSpriteNode.spriteFrame = new cc.SpriteFrame(texture);
            });
        }
        else if (number === 1) {
            const photoUrl = self.fbMatchGame.GetMathPlayerPhoto();
            cc.loader.load(photoUrl, (err, texture) => {
                self.opponentPhotoSpriteNode.spriteFrame = new cc.SpriteFrame(texture);
            });
        }
    },

    playButtonAction () {
        this.botSensorNode.active = false;
        this.objectContainerNode.removeAllChildren();
        this.ballContainerNode.removeAllChildren();
        this.gameScreenNode.active = true;
        this.node.active = false;
        this.gameOverProcessing.switchingisMatching(true);
    },

    returnButtonAction () {
        this.titleScreenNode.active = true;
        this.node.active = false;
    }

    //start () {},

    // update (dt) {},
});
