const DataManager = require("DataManager");

cc.Class({
    extends: cc.Component,

    properties: {
        dataManager: DataManager,

        objetHitParticle: cc.Prefab,
        ballShockWaveOarticle: cc.Prefab,
        ballEndParticle: cc.Prefab,
        blockEndParticle: cc.Prefab,
        diamondFallParticle: cc.Prefab,
        diamondShockWaveParticle: cc.Prefab,

        _particleInstans: null,
    },

    // LIFE-CYCLE CALLBACKS:

    particleCreate (particleNumber, setPositionX, setPositionY) {
        switch(particleNumber) {
            //ボールが何かに接触
            case 0:
            this._particleInstans = cc.instantiate(this.objetHitParticle);
            break;
            //ボールがブロックに接触
            case 1:
            this._particleInstans = cc.instantiate(this.ballShockWaveOarticle);
            break;
            //ボールが天井に接触
            case 2:
            this._particleInstans = cc.instantiate(this.ballEndParticle);
            break;
            //ブロックが床に接触
            case 3:
            this._particleInstans = cc.instantiate(this.blockEndParticle);
            break;
            //ダイヤ崩壊
            case 4:
            this._particleInstans = cc.instantiate(this.diamondFallParticle);
            break;
            //ダイヤがボールに接触
            case 5:
            this._particleInstans = cc.instantiate(this.diamondShockWaveParticle);
            break;
        }

        let parentNode = this.node;
        parentNode.addChild(this._particleInstans, 0, 0);
        this._particleInstans.setPosition(cc.v2(setPositionX, setPositionY));
    }
});
