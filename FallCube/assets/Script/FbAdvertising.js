/*
    このスクリプトについて

    現在リワード広告とインタースティシャル広告の2種類が対応しています。
    Prefabsフォルダにあるこのスクリプトと同名のプレハブをNodeTreeに設置(このスクリプトを直接NodeTreeにD&Dしてもok)して、
    このスクリプト内のadRewardedVideoIDとadInterstitialIDにそれぞれのIDをセットすると広告が読み込まれます。
    インタースティシャル広告に関しては40%の確率で読み込みを行います。
    あとはそれぞれの表示させたいタイミングでshowRewardedAdとshowInterstitialAdを呼び出して下さい。
*/

cc.Class({
    extends: cc.Component,

    properties: {
        _preLoadedAdRewardedVideo: null,
        _preLoadedAdInterstitial: null,
        //リワード広告ID
        adRewardedVideoID: '440507510065764_440510693398779',
        //インタースティシャル広告ID
        adInterstitialID: '440507510065764_440510823398766',
        //インタースティシャル広告表示確率
        _adInterstitialDisplayProbability: null,

        resultScreenNode: cc.Node,
        reawrdScreenNode: cc.Node,
        debugLabel: cc.Label
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        //乱数セット(0〜9)
        this._adInterstitialDisplayProbability = Math.floor( Math.random () * 10) + 1;
        console.log('_adInterstitialDisplayProbability:' + this._adInterstitialDisplayProbability);

        this.adRewardedLoad();

        //40％の確率でインタースティシャル広告読み込み
        if (this._adInterstitialDisplayProbability < 5)
        {
            this.adInterstitialLoad();
        }
    },

    //リワード広告の読み込み
    adRewardedLoad: function () {
        const self = this;
        console.log('_preLoadedAdRewardedVideo:' + self._preLoadedAdRewardedVideo);
        if (typeof FBInstant == 'undefined' || self._preLoadedAdRewardedVideo != null) return;
        console.log('リワード読み込み開始');
        //self.debugLabel.string = "リワード読み込み開始";
        FBInstant.getRewardedVideoAsync(self.adRewardedVideoID).then(function(rewarded) {
                console.log('リワード読み込み中');
                //self.debugLabel.string = "リワード読み込み中";
                self._preLoadedAdRewardedVideo = rewarded;
                console.log(self._preLoadedAdRewardedVideo);
                return self._preLoadedAdRewardedVideo.loadAsync();
            }).then(function() {
                console.log('リワード読み込み完了');
                //self.debugLabel.string = "リワード読み込み完了";
            }).catch(function(error){
                console.error('リワードの読み込みに失敗しました：' + error.message);
                //self.debugLabel.string = "リワードの読み込みに失敗しました:\n" + error.message;
                if (error.message === 'No fill') {
                    self._preLoadedAdRewardedVideo = null;
                    //self.debugLabel.string = "ReLoad";
                    FBInstant.getRewardedVideoAsync(self.adInterstitialID).then(function(rewarded) {
                        self._preLoadedAdRewardedVideo = rewarded;
                        return self._preLoadedAdRewardedVideo.loadAsync();
                    }).then(function() {
                        //self.debugLabel.string = "リワード読み込み完了";
                    }).catch(function(error) {
                        //self.debugLabel.string = error.message;
                        //self.debugLabel.string = 'test';
                        self._preLoadedAdRewardedVideo = null;
                    })
                }
                else {
                    self._preLoadedAdRewardedVideo = null;
                }
                console.log(self.adRewardedVideoID);
        });
    },

    //インタースティシャル広告読み込み
    adInterstitialLoad: function () {
        const self = this;
        console.log('_preLoadedAdInterstitial:' + self._preLoadedAdInterstitial);
        if (typeof FBInstant == 'undefined' || self._preLoadedAdInterstitial != null) return;
        console.log('インタースティシャル読み込み開始');
        FBInstant.getInterstitialAdAsync(self.adInterstitialID).then(function(interstitial) {
                console.log('インタースティシャル読み込み');
                self._preLoadedAdInterstitial = interstitial;
                console.log(self._preLoadedAdInterstitial);
                return self._preLoadedAdInterstitial.loadAsync();
            }).then(function () {
                console.log('インタースティシャル読み込み完了')
            }).catch(function (error) {
                console.error('広告の読み込みに失敗しました：' + error.message);
                console.log(self.adInterstitialID);
        });
    },

    //広告動画再生
    showRewardedAd: function() {
        const self = this;
        if (self._preLoadedAdRewardedVideo == null) return;
        //self.debugLabel.string = "ShowReward";
        //正常に動画を読み込んでいると再生する
        self._preLoadedAdRewardedVideo.showAsync().then(function() {
            console.log('リワード動画再生');
            //self.debugLabel.string = "リワード動画再生";
            self._preLoadedAdRewardedVideo = null;
            self.reawrdScreenNode.active = true;
            self.resultScreenNode.active = false;
        }).catch(function(error) {
            console.error(error.message);
            //self.debugLabel.string = error.message;
        });
    },

    //インタースティシャル広告表示
    showInterstitialAd: function () {
        const self = this;
        if (self._preLoadedAdInterstitial == null) return;
        //正常に広告を読み込んでいると表示
        self._preLoadedAdInterstitial.showAsync().then(function () {
            console.log('インタースティシャル広告表示');
            self._preLoadedAdInterstitial = null;
        }).catch(function (error) {
            console.log(error.message);
        });
    },

    adRewardedLoadCheckr: function () {
        if (this._preLoadedAdRewardedVideo !== null &&　this._preLoadedAdRewardedVideo !== 'undefined') {
            return true;
        }
        else {
            return false;
        }
    },

    returnrewardInstans () {
        return this._preLoadedAdRewardedVideo;
    }
});
