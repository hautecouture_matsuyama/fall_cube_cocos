cc.Class({

    extends: cc.Component,

    properties: {
        _rigidbody: null,
        _moveSpeed: -450,
        _isSlowMotion: false,

        _audioManagerNode: null,
        _audioManager: null,
    },
    
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this._rigidbody = this.node.getComponent(cc.RigidBody);
        this._rigidbody.linearVelocity = cc.v2(0, this._moveSpeed);

        this._audioManagerNode = cc.find('Canvas/AudioManager');
        this._audioManager = this._audioManagerNode.getComponent('AudioManager');
    },

    update (dt) {
        if (this._isSlowMotion === false && cc.director.getScheduler().getTimeScale() < 1) {
            this._isSlowMotion = true;
            this._moveSpeed = this._moveSpeed / 2;
            this._rigidbody.linearVelocity = cc.v2(0, this._moveSpeed);
        }
        else if (this._isSlowMotion === true && cc.director.getScheduler().getTimeScale() === 1){
            this._isSlowMotion = false;
            this._moveSpeed = this._moveSpeed * 2;
            this._rigidbody.linearVelocity = cc.v2(0, this._moveSpeed);
        }
    },

    onBeginContact: function (contact, self, other) {
        switch (other.tag) {
            case 0:
            case 9:
            this.node.destroy();
            break;

            case 10:
            this._audioManager.audioPlay(0);
            break;
        }
    },

    onDestroy () {
        this._audioManager.audioStop();
    }

    // update (dt) {},
});
