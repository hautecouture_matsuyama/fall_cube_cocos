const DataManager = require("DataManager");
const FbMatchGame = require("FbMatchGame");

cc.Class({
    extends: cc.Component,

    properties: {
        dataManager: DataManager,
        fbMatchGame: FbMatchGame,

        resultScreenNode: cc.Node,

        winTextLabel: cc.Node,
        losingTextLabel: cc.Node,
        drawTextLabel: cc.Node,
        crownSprite: cc.Node,

        myPhotoSpriteNode: cc.Sprite,
        myScoreLabel: cc.Label,

        opponentPhotoSpriteNode: cc.Sprite,
        opponentNameLabel: cc.Label,
        opponentScoreLabel: cc.Label,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.photoChange(0);
        this.myScoreLabel.string = this.dataManager.returnScore() + ' Point';

        this.photoChange(1);
        this.opponentNameLabel.string = this.fbMatchGame.GetMathPlayerName();
        this.opponentScoreLabel.string = this.fbMatchGame.GetMathScore() + ' Point';

        switch (this.fbMatchGame.GetMathResult()) {
            //勝ち
            case 0:
            this.winTextLabel.active = true;
            this.crownSprite.setPosition(cc.v2(-175, 85));
            this.crownSprite.active = true;
            break;

            //負け
            case 1:
            this.losingTextLabel.active = true;
            this.crownSprite.setPosition(cc.v2(175, 85));
            this.crownSprite.active = true;
            break;

            //引き分け
            case 2:
            this.drawTextLabel.active = true;
            break;
        }
    },

    photoChange (number) {
        const self = this;
        if (number === 0) {
            const photoUrl = self.fbMatchGame.GetMyPhoto();
            cc.loader.load(photoUrl, (err, texture) => {
                self.myPhotoSpriteNode.spriteFrame = new cc.SpriteFrame(texture);
            });
        }
        else if (number === 1) {
            const photoUrl = self.fbMatchGame.GetMathPlayerPhoto();
            cc.loader.load(photoUrl, (err, texture) => {
                self.opponentPhotoSpriteNode.spriteFrame = new cc.SpriteFrame(texture);
            });
        }
    },

    moveOnButtonAction () {
        this.resultScreenNode.active = true;
        this.node.active = false;
    }

    // update (dt) {},
});
