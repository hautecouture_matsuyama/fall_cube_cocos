const ObjectCreator = require("ObjectCreator");

cc.Class({
    extends: cc.Component,

    properties: {
        objectCreator: ObjectCreator,

        _ballCreatePositionX: 0,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.schedule(this.botBlockCreate, 2, cc.REPEAT_FOREVER);
    },

    onBeginContact (contact, self, other) {
        this._ballCreatePositionX = other.node.getPositionX();
        switch (other.tag) {
            case 1:
            this.botBallCreate();
            break;

            case 2:
            this.schedule(this.botBallCreate, 0.2, 1);
            break;

            case 3:
            this.schedule(this.botBallCreate, 0.2, 2);
            break;

            case 4:
            this.schedule(this.botBallCreate, 0.2, 3);
            break;

            case 5:
            this.schedule(this.botBallCreate, 0.2, 4);
            break;
        }
    },

    botBlockCreate () {
        this.objectCreator.randomObjectCreate(true);
    },

    botBallCreate () {
        this.objectCreator.ballCreate(this._ballCreatePositionX);
    },

    onDisable () {
        this.unschedule(this.botBlockCreate);
    }
});
