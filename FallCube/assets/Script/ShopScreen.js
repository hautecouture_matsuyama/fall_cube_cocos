const ShopItem = require("ShopItem");
const DataManager = require("DataManager");
const AudioManager = require("AudioManager");

cc.Class({
    extends: cc.Component,

    properties: {
        dataManager: DataManager,
        audioManager: AudioManager,

        titleScreenNode: cc.Node,
        requiredItemNode: cc.Node,
        requiredDiamondLabel: cc.Label,
        righitArrowButton: cc.Node,
        righitArrowButton3: cc.Node,
        leftArrowButton: cc.Node,
        leftArrowButton3: cc.Node,
        playButton: cc.Node,
        unLockButton: cc.Node,
        shopItemNode: cc.Node,
        shopItems: [ShopItem],

        //商品配列ポインタ
        _pointer: 0,
    },

    // LIFE-CYCLE CALLBACKS:

    righitArrowButtonAction () {
        if (this._pointer === 14) return;
        this.ballShrink();
        this.shopItemNode.x -= 200;
        this._pointer += 1;
        this.ballExpansion();
    },

    righitArrow3ButtonAction () {
        if (this._pointer === 14) return;
        this.ballShrink();
        this.shopItemNode.x -= 600;
        if (this.shopItemNode.x <= -2800) {
            this.shopItemNode.x = -2800;
        }
        this._pointer += 3;
        if (this._pointer >= 14) {
            this._pointer = 14;
        }
        this.ballExpansion();
    },

    leftArrowButtonAction () {
        if (this._pointer === 0) return;
        this.ballShrink();
        this.shopItemNode.x += 200;
        this._pointer -= 1;
        this.ballExpansion();
    },

    leftArrow3ButtonAction () {
        if (this._pointer === 0) return;
        this.ballShrink();
        this.shopItemNode.x += 600;
        if (this.shopItemNode.x >= 0) {
            this.shopItemNode.x = 0;
        }
        this._pointer -= 3;
        if (this._pointer <= 0) {
            this._pointer = 0;
        }
        this.ballExpansion();
    },

    ballShrink () {
        this.shopItems[this._pointer].ball.width = 100;
        this.shopItems[this._pointer].ball.height = 100;
    },

    ballExpansion () {
        this.shopItems[this._pointer].ball.width = 200;
        this.shopItems[this._pointer].ball.height = 200;

        this.shopUISwitching();
    },

    //ShopUI項目の表示切り替え
    shopUISwitching () {
        this.shopItems[this._pointer].isUnlocked = this.dataManager.returnBallUnLock(this.shopItems[this._pointer].ball.name);

        //ボールのロック状態を引っ張ってきてUIの表示を切り替える
        if (this.shopItems[this._pointer].isUnlocked)
        {
            this.unLockButton.active = false;
            this.playButton.active = true;
            this.requiredItemNode.active = false;
        }
        else
        {
            this.playButton.active = false;
            this.unLockButton.active = true;
            this.requiredDiamondLabel.string = this.shopItems[this._pointer].itemPrice;
            this.requiredItemNode.active = true;
        }

        if (this._pointer === 0) {
            this.leftArrowButton.active = false;
            this.leftArrowButton3.active = false;
        }
        else {
            this.leftArrowButton.active = true;
            this.leftArrowButton3.active = true;
        }

        if (this._pointer === 14) {
            this.righitArrowButton.active = false;
            this.righitArrowButton3.active = false;
        }
        else {
            this.righitArrowButton.active = true;
            this.righitArrowButton3.active = true;
        }
    },

    playButtonAction () {
        this.audioManager.audioPlay(3);
        console.log(this.shopItems[this._pointer].node.color);
        this.dataManager.changeSelectedBallColor(this.shopItems[this._pointer].node.color);
        this.titleScreenNode.active = true;
        this.node.active = false;
    },

    unLockButtonAction () {
        //ダイヤの所持数で条件分岐
        if (this.dataManager.returnPossessedDiamond() >= this.shopItems[this._pointer].itemPrice)
        {
            console.log("BallGet");
            this.audioManager.audioPlay(3);
            //選択しているボールのロック状態を解除
            this.shopItems[this._pointer].isUnlocked = true;
            this.dataManager.switchingBallUnLock(this.shopItems[this._pointer].ball.name, true);
            this.dataManager.consumeDiamond(this.shopItems[this._pointer].itemPrice);
            this.shopUISwitching();
        }
        else {
            console.log("test");
        }
    },
});
