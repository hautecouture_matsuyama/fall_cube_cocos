const TitleScreen = require("TitleScreen");

cc.Class({
    extends: cc.Component,

    properties: {
        titleScreen: TitleScreen,

        clockAudio: {
            default: null,
            url: cc.AudioClip,
        },
        diamondAudio: {
            default: null,
            url: cc.AudioClip,
        },
        knockAudio: {
            default: null,
            url: cc.AudioClip,
        },
        bubbleAudio: {
            default: null,
            url: cc.AudioClip,
        },
        shotAudio: {
            default: null,
            url: cc.AudioClip,
        }
    },

    // LIFE-CYCLE CALLBACKS:

    gameStartAudioPlay () {
        cc.audioEngine.playEffect(this.bubbleAudio, false);
    },

    audioPlay (audioNumber) {
        if (this.titleScreen.returnisMute() === true) return;
        switch (audioNumber) {
            case 0:
            cc.audioEngine.playEffect(this.clockAudio, false);
            break;
            case 1:
            cc.audioEngine.playEffect(this.diamondAudio, false);
            break;
            case 2:
            cc.audioEngine.playEffect(this.knockAudio, false);
            break;
            case 3:
            cc.audioEngine.playEffect(this.bubbleAudio, false);
            break;
            case 4:
            cc.audioEngine.playEffect(this.shotAudio, false);
            break;
        }
    },

    audioStop () {
        if (this.titleScreen.returnisMute() === true) return;
        cc.audioEngine.stopAllEffects();
    }
});
