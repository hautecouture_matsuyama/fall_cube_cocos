const GameOverProcessing = require("GameOverProcessing");

cc.Class({
    extends: cc.Component,

    properties: {
        gameOverProcessing: GameOverProcessing
    },

    // LIFE-CYCLE CALLBACKS:

    onBeginContact: function (contact, self, other) {
        if (this.node.name === "GameOverCeiling" && other.tag === 0) {
            this.gameOverProcessing.isGameOver();
        }
        else if (this.node.name === "GameOverFloor") {
            switch (other.tag) {
                case 6:
                case 7:
                case 8:
                break;

                default:
                this.gameOverProcessing.isGameOver();
                break;
            }
        }
    },
});
