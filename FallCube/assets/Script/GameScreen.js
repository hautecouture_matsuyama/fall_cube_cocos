const DataManager = require("DataManager");
const ObjectCreator = require("ObjectCreator");

cc.Class({
    extends: cc.Component,

    properties: {
        dataManager: DataManager,
        objectCreator: ObjectCreator,

        canvas: cc.Node,

        //最初にタッチした座標
        _touchStartPisition: 0,

        _slowTimer: 5,
        _isSlowMotion: false,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.schedule(this.objectCreate, 0.75, cc.REPEAT_FOREVER);

        const self = this;
        self.isTouching = false;

        //タッチ開始時処理
        self.canvas.on(cc.Node.EventType.TOUCH_START, function(event) {
            self.isTouching = true;
            const touches = event.getTouches();
            self._touchStartPisition = touches[0].getLocationX();
            if (cc.sys.isMobile) {
                self._touchStartPisition = self._touchStartPisition - 375;
            }
            else {
                self._touchStartPisition = self._touchStartPisition - 375;
            }
            //タッチした座標を送ってボール生成
            self.objectCreator.ballCreate(self._touchStartPisition);
        }, self.node);
    },

    update (dt) {
        if (this._isSlowMotion === true) {
            this._slowTimer -= dt;
            if (this._slowTimer <= 0){
                this.gameSpeedChange();
            }
        }
    },

    slowMotionStart () {
        //if (this.node.active === false) return;
        if (this._isSlowMotion === true) return;
        this._isSlowMotion = true;
        cc.director.getScheduler().setTimeScale(0.25);
    },

    gameSpeedChange () {
        this._isSlowMotion = false;
        this._slowTimer = 5;
        cc.director.getScheduler().setTimeScale(1.0);
    },

    objectCreate () {
        this.objectCreator.randomObjectCreate(false);
    }
});
