let FallCubeUserData = {
    //ベストスコア
    bestScore: 0,
    //ダイヤ所持数
    possessedDiamond: 0,
    //リワードを再生したか
    isWatchReward: false,
    //ショートカットを作成したか
    isCreateShortcut: false,
    //使用するボールの色
    selectedBallColor: cc.color(255, 255, 255, 255),
    //各ボールの購入状態
    whiteBallUnlock: true,
    blackBallUnlock: false,
    redBallUnlock: false,
    greenBallUnlock: false,
    buleBallUnlock: false,
    yellowBallUnlock: false,
    vermilionBallUnlock: false,
    aquaBallUnlock: false,
    lightBallUnlock: false,
    orangeBallUnlock: false,
    mothBallUnlock: false,
    grayBallUnlock: false,
    purpleBallUnlock: false,
    pinkBallUnlock: false,
    amberBallUnlock: false,
};

cc.Class({
    extends: cc.Component,

    properties: {
        shopPossessedDiamondLabel: cc.Label,
        gamePossessedDiamondLabel: cc.Label,
        gameOverPossessedDiamondLabel: cc.Label,
        gameScoreLabel: cc.Label,
        resultScoreLabel: cc.Label,
        bestScoreLabel: cc.Label,

        _gameScore: 0,

        //セーブデータとして残すユーザーデータ
        _fallCubeUserData: null
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.readUserData();

        this.shopPossessedDiamondLabel.string = this._fallCubeUserData.possessedDiamond;
        this.gamePossessedDiamondLabel.string = this._fallCubeUserData.possessedDiamond;
        this.gameOverPossessedDiamondLabel.string = this._fallCubeUserData.possessedDiamond;
    },

    //ユーザーデータ読み込み
    readUserData () {
        var data = cc.sys.localStorage.getItem('FallCubeUserData');
        //確認用ログ
        cc.log(data);

        if (data !== null) {
            //見つかったらセーブファイルのデータを適用
            this._fallCubeUserData = JSON.parse(data);
        }
        else {
            //見つからなかったら初期値をセット
            this._fallCubeUserData = FallCubeUserData;
        }

        console.log(this._fallCubeUserData.bestScore);
    },

    //ユーザーデータの記録
    writingUseData () {
        //JOSNオブジェクトにして保存
        cc.sys.localStorage.setItem('FallCubeUserData',JSON.stringify(this._fallCubeUserData));
    },

    //各ボールのアンロック状態を返す
    returnBallUnLock (ballName) {
        switch (ballName) {
            case "WhiteBall":
            return this._fallCubeUserData.whiteBallUnlock;

            case "BlackBall":
            return this._fallCubeUserData.blackBallUnlock;

            case "RedBall":
            return this._fallCubeUserData.redBallUnlock;

            case "GreenBall":
            return this._fallCubeUserData.greenBallUnlock;

            case "BuleBall":
            return this._fallCubeUserData.buleBallUnlock;

            case "YellowBall":
            return this._fallCubeUserData.yellowBallUnlock;

            case "VermillionBall":
            return this._fallCubeUserData.vermilionBallUnlock;

            case "AquaBall":
            return this._fallCubeUserData.aquaBallUnlock;

            case "LightGreenBall":
            return this._fallCubeUserData.lightBallUnlock;

            case "OrangeBall":
            return this._fallCubeUserData.orangeBallUnlock;

            case "MothBall":
            return this._fallCubeUserData.mothBallUnlock;

            case "GrayBall":
            return this._fallCubeUserData.grayBallUnlock;

            case "PurpleBall":
            return this._fallCubeUserData.purpleBallUnlock;

            case "PinkBall":
            return this._fallCubeUserData.pinkBallUnlock;

            case "AmberBall":
            return this._fallCubeUserData.amberBallUnlock;
        }
    },

    //各ボールのアンロック状態を変更
    switchingBallUnLock (ballName, unlockBallFlg) {
        console.log(ballName+':'+unlockBallFlg);
        switch (ballName) {
            case "WhiteBall":
            this._fallCubeUserData.whiteBallUnlock = unlockBallFlg;
            break;

            case "BlackBall":
            this._fallCubeUserData.blackBallUnlock = unlockBallFlg;
            break;

            case "RedBall":
            this._fallCubeUserData.redBallUnlock = unlockBallFlg;
            break;

            case "GreenBall":
            this._fallCubeUserData.greenBallUnlock = unlockBallFlg;
            break;

            case "BuleBall":
            this._fallCubeUserData.buleBallUnlock = unlockBallFlg;
            break;

            case "YellowBall":
            this._fallCubeUserData.yellowBallUnlock = unlockBallFlg;
            break;

            case "VermillionBall":
            this._fallCubeUserData.vermilionBallUnlock = unlockBallFlg;
            break;

            case "AquaBall":
            this._fallCubeUserData.aquaBallUnlock = unlockBallFlg;
            break;

            case "LightGreenBall":
            this._fallCubeUserData.lightBallUnlock = unlockBallFlg;
            break;

            case "OrangeBall":
            this._fallCubeUserData.orangeBallUnlock = unlockBallFlg;
            break;

            case "MothBall":
            this._fallCubeUserData.mothBallUnlock = unlockBallFlg;
            break;

            case "GrayBall":
            this._fallCubeUserData.grayBallUnlock = unlockBallFlg;
            break;

            case "PurpleBall":
            this._fallCubeUserData.purpleBallUnlock = unlockBallFlg;
            break;

            case "PinkBall":
            this._fallCubeUserData.pinkBallUnlock = unlockBallFlg;
            break;

            case "AmberBall":
            this._fallCubeUserData.amberBallUnlock = unlockBallFlg;
            break;
        }
    },

    //ショートカット作成のリワード
    createShortcutReward () {
        this.getDiamond(40);
        this._fallCubeUserData.isCreateShortcut = true;
        this.writingUseData();
    },

    returnisCreateShortcut () {
        return this._fallCubeUserData.isCreateShortcut;
    },

    //使用するボールの色変更
    changeSelectedBallColor (ballColor) {
        this._fallCubeUserData.selectedBallColor = ballColor;
        this.writingUseData();
    },

    returnBallColor () {
        return this._fallCubeUserData.selectedBallColor;
    },

    returnScore () {
        return this._gameScore;
    },

    returnBestScore () {
        return this._fallCubeUserData.bestScore;
    },

    returnPossessedDiamond () {
        return this._fallCubeUserData.possessedDiamond;
    },

    //スコア更新
    scoreUpdate (score) {
        this._gameScore = this._gameScore + score;
        console.log(this._fallCubeUserData.bestScore);
        if (this._fallCubeUserData.bestScore < this._gameScore) {
            this._fallCubeUserData.bestScore = this._gameScore;
        }

        this.gameScoreLabel.string = this._gameScore;
        this.resultScoreLabel.string = this._gameScore;
        this.bestScoreLabel.string = this._fallCubeUserData.bestScore;
    },

    //ダイヤを入手した
    getDiamond (num) {
        //リワード動画再生で獲得数倍増
        if (this._fallCubeUserData.isWatchReward === true) {
            num = (num + 1)|0;
        }
        this._fallCubeUserData.possessedDiamond += num|0;
        this.shopPossessedDiamondLabel.string = this._fallCubeUserData.possessedDiamond;
        this.gamePossessedDiamondLabel.string = this._fallCubeUserData.possessedDiamond;
        this.gameOverPossessedDiamondLabel.string = this._fallCubeUserData.possessedDiamond;

        this.writingUseData();
    },

    //ダイヤを消費した
    consumeDiamond (num) {
        this._fallCubeUserData.possessedDiamond -= num|0;
        this.shopPossessedDiamondLabel.string = this._fallCubeUserData.possessedDiamond;
        this.gamePossessedDiamondLabel.string = this._fallCubeUserData.possessedDiamond;
        this.gameOverPossessedDiamondLabel.string = this._fallCubeUserData.possessedDiamond;

        this.writingUseData();
    },

    iswatchRewardSwitch (flg, instans) {
        this._fallCubeUserData.isWatchReward = flg;
        this.writingUseData();
    }
});
