cc.Class({
    extends: cc.Component,

    properties: {
        blockLifeLabel: cc.Label,
        blockCollider: cc.PhysicsBoxCollider,
        blockFallParticle1: cc.Node,
        blockFallParticle2: cc.Node,

        _botSensorNode: null,
        
        _blockBaseColor: cc.Color,
        _blockChangeColor: cc.Color,
        _backGroundNode: null,
        _colorSwitchBackGround: null,

        _dataManagerNode: null,
        _dataManager: null,

        _particleCreatorNode: null,
        _particleCreator: null,

        _rigidbody: null,
        _moveSpeed: 0,

        _blockLife: 0,

        _isLife0: false,
        _isSlowMotion: false,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this._dataManagerNode = cc.find('Canvas/DataManager');
        this._dataManager = this._dataManagerNode.getComponent('DataManager');

        this._botSensorNode = cc.find('Canvas/ScreenCommonNode/BotSensor');
        if (this._botSensorNode.active === true) {
            this._blockLife = Math.floor(Math.random() * 2 + 1);
        }
        else {
            //スコアの獲得状況でブロックのライフ設定
            if (this._dataManager.returnScore() < 5) {
                this._blockLife = 1;
            }
            else if (this._dataManager.returnScore() < 15) {
                this._blockLife = Math.floor(Math.random() * 2 + 1);
            }
            else if (this._dataManager.returnScore() < 25) {
                this._blockLife = Math.floor(Math.random() * 3 + 1);
            }
            else if (this._dataManager.returnScore() < 35) {
                this._blockLife = Math.floor(Math.random() * 4 + 1);
            }
            else if (this._dataManager.returnScore() > 45) {
                this._blockLife = Math.floor(Math.random() * 5 + 1);
            }
        }

        try {
            this.blockLifeLabel.string = this._blockLife;
            this.blockCollider.tag = this._blockLife;
        } catch (error) {
            console.log(error);
            this.node.destroy();
        }

        this._blockBaseColor = this.node.color;
        this._backGroundNode = cc.find('Canvas/ScreenCommonNode/BackGroundNode');

        this._particleCreatorNode = cc.find('Canvas/ScreenCommonNode/ParticleCreatorNode');
        this._particleCreator = this._particleCreatorNode.getComponent('ParticleCreator');

        this._colorSwitchBackGround = this._backGroundNode.getComponent('ColorSwitchBackground');
        this._rigidbody = this.node.getComponent(cc.RigidBody);

        switch (this._blockLife) {
            case 1:
            this._rigidbody.fixedRotation = false;
            this._moveSpeed = 450;
            break;

            case 2:
            this._moveSpeed = 450;
            break;

            case 3:
            this._moveSpeed = 200;
            break;

            case 4:
            this._moveSpeed = 200;
            break;

            case 5:
            this._moveSpeed = 200;
            break;
        }

        this._rigidbody.linearVelocity = cc.v2(0, -1 * this._moveSpeed);
    },

    onBeginContact (contact, self, other) {
        switch (other.tag) {
            //ボール
            case 0:
            if (this._blockLife <= 0) break;
            this._blockLife -= 1;
            this.blockLifeLabel.string = this._blockLife;

            if (this._blockLife <= 0) {
                this._moveSpeed = 0;
                this._rigidbody.linearVelocity = cc.v2(0, 0);
            }
            break;

            //削除壁
            case 9:
            this.node.destroy();
            break;

            case 11:
            this._moveSpeed = 0;
            this._rigidbody.linearVelocity = cc.v2(0, 0);
            this.blockFallParticle1.active = false;
            this.blockFallParticle2.active = false;
            const particleSetPositionX = this.node.getPositionX();
            const particleSetPositionY = this.node.getPositionY();
            this._particleCreator.particleCreate(3, particleSetPositionX, particleSetPositionY);
            break;
        }
    },

    onEndContact: function (contact, self, other) {
        if (other.tag === 0)
        {
            if (this._isLife0 === true) return;

            if (this._blockLife <= 0) {
                this.blockCollider.tag = 6;
                this.blockFallParticle1.active = false;
                this.blockFallParticle2.active = false;

                const randomX = Math.floor( Math.random() * 100) - 50;
                const randomY = Math.round( Math.random() * 100 ) + 30;


                this._rigidbody.linearVelocity = cc.v2(randomX * 1500, randomY * 1500);
                this._isLife0 = true;
            }
        }
    },

    update (dt) {
        this.colorChange();

        if (this._isSlowMotion === false && cc.director.getScheduler().getTimeScale() < 1) {
            if (this._blockLife >= 1) {
                this._isSlowMotion = true;
                this._moveSpeed = this._moveSpeed / 2;
                if (this._blockLife <= 0) return;
                this._rigidbody.linearVelocity = cc.v2(0, -1 * this._moveSpeed);
            }
        }
        else if (this._isSlowMotion === true && cc.director.getScheduler().getTimeScale() === 1) {
            this._isSlowMotion = false;
            this._moveSpeed = this._moveSpeed * 2;
            if (this._blockLife <= 0) return;
            this._rigidbody.linearVelocity = cc.v2(0, -1 * this._moveSpeed);
        }
    },

    colorChange () {
        const color = this._colorSwitchBackGround.returnUnderBackGroundColor();
        let red = this._blockBaseColor.r + (color.r / 2);
        if (red >= 255)
        {
            red = 255;
        }
        let green = this._blockBaseColor.g + (color.g / 2);
        if (green >= 255)
        {
            green = 255;
        }
        let bule = this._blockBaseColor.b + (color.b / 2);
        if (bule >= 255)
        {
            bule = 255;
        }
        this._blockChangeColor.r = red;
        this._blockChangeColor.g = green;
        this._blockChangeColor.b = bule;

        this.node.color = this._blockChangeColor;
    }
});
